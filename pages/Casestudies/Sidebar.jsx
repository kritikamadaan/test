import Head from 'next/head'

export const config = { amp: true };

export default function Slider(props) {
  return (<div>
    <React.Fragment>
      <Head>
        <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

      </Head>

      <amp-sidebar id="sidebar1" layout="nodisplay" side="right">
        <ul>
          <li>Nav item 1</li>
          <li><a href="#idTwo" on="tap:idTwo.scrollTo">Nav item 2</a></li>
          <li>Nav item 3</li>
          <li><a href="#idFour" on="tap:idFour.scrollTo">Nav item 4</a></li>
          <li>Nav item 5</li>
          <li>Nav item 6</li>
        </ul>
        hell of from sidebar
                </amp-sidebar>

      <amp-sidebar id="sidebar" layout="nodisplay" side="right">
        <ul>

          <li>Home</li>
          <li>About</li>
          <li>Career</li>
          <li>work with us</li>
          <li>Contact us</li>
        </ul>
        <button on="tap:sidebar.close"> close sidebar</button>


      </amp-sidebar>
      <button on="tap:sidebar.open">Open sidebar</button>
      <style jsx>{`
body {margin:0;}

ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
  position: fixed;
  top: 0;
  width: 100%;
}

li {
  float: left;
}

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #111;
}

.active {
  background-color: #4CAF50;
}
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}

      `}</style>



    </React.Fragment>




  </div>);






}
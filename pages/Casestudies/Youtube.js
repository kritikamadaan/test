import Head from 'next/head'

export const config = { amp: true };

export default function Youtube(props) {
    return (<div>
        <React.Fragment>

            <Head>
                <script async src="https://cdn.ampproject.org/v0.js"></script>
                <script custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js" async></script>
                <link rel="canonical" href="https://preview.amp.dev/documentation/guides-and-tutorials/develop/media_iframes_3p/third_party_components.example.3.html" />
                {/*<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1" />*/}
            </Head>
            <body>


                <amp-youtube data-videoid="lBTCB7yLs8Y"
                    layout="responsive"
                    width="560"
                    height="315">
                </amp-youtube>

            </body>
        </React.Fragment>




    </div>);






}
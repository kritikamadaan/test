import React, { Component } from 'react';
import Navbar from './Navbar';
import Slider from "./Slider";
import Accordion from './Accordion';

import Youtube from './Youtube';
import Sidebar from "./Sidebar";
import Footers from "./Footers";
import Content from './Content';
import Common from "./Common";

class Casestudies extends Component {
    render() {
        return (
            <div>

                <Navbar />
                <Sidebar />

                <Slider />
                <Common  name="DYANAMIC WEB SOLUTIONS" paragraph="The world of technology is moving quite briskly and we want you to keep the pace. That is why we bring to the dynamic web solutions so that you can match the expectations of your clients at every step." />
                <Common  name="BIG DATA" paragraph="When Big Data platforms are effectively captured and analyzed, organizations gain a clear and complete understanding of their business which would lead to lower costs, efficiency improvements, increased sales and better customer service.
</p>" />
                <Common  name="DEVOPS AND TESTING" paragraph="To be victorious in the current dynamic environment, organizations need to possess incredible agility to address the varying needs of their customers. The diverse functions within every venture need to be functioning lucidly to orchestrate these changes and to make sure that they reach the customers quicker than the competition. To answer these challenges, businesses adopt ‘Agile’ software development processes.
                        
                                        Our agile methodology includes making expert esteem streams inside the business through cooperative energy among improvement and tasks. A definitive objective is to make quicker conveyance through computerization, prior coordinated effort among tasks and advancement, and building quality in through testing and quality control measure" />

            
                <Content />

                <Accordion />
                <Youtube />
                <Footers />
            </div>
        );
    }
}

export default Casestudies;

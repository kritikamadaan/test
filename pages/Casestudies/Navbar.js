import React, { Component } from 'react';
import Link from "next/link";
import Sidebar from "./Sidebar";

class Navbar extends Component {
  render() {
    return (
      <div>


        <ul>
          <li>
            <Link href="/Casestudies">
              <a>Home</a>
            </Link>
          </li>
          <li>
            <Link href="/Casestudies">
              <a>About Us</a>
            </Link>
          </li>
          <li>
            <Link href="/Casestudies">
              <a>contact us</a>
            </Link>
          </li>
          <li>
            <Link href="/Casestudies">
              <a>who are we</a>
            </Link>
          </li>
          <li>
            <Link href="/Casestudies">
              <a><Sidebar /></a>
            </Link>
          </li>
        </ul>
        <Sidebar />

        <style jsx>{`
body {margin:0;}

ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
  position: fixed;
  top: 0;
  width: 100%;
}

li {
  float: left;
}

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #111;
}

.active {
  background-color: #4CAF50;
}
      `}</style>





      </div>
    );
  }
}

export default Navbar;

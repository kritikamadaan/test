import React from "react";
import Link from "next/link";

function Common(props) {

    return (
        <div className="container">
            <section className="what-we-do-section">
                <div className="container">
                    <div className="row">
                        <div className="agile-development wwd">
                            <h2 className="heading">WHAT TO DO</h2>
                        </div>
                        <div className="col-sm-4">
                            <div className="frame">
                                <div className="group">
                                    <img src="images/Untitled-1.png" />
                                </div>
                                <div className="dyanamic-web-solutions">
                                    <h4>{props.name}</h4>
                                </div>
                                <div className="at-spineor-we-explore-learn-an">
                                    <p>{props.paragraph}
                                    </p>
                                </div>
                                <div className="view-more">
                                    <Link href="/Dynamic-web-solutions"><a className="frame1" >VIEW MORE</a></Link>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <style global jsx>{`
                .dyanamic-web-solutions h4 {
    font-weight: 600;
    color: #4A4A4A;
    fontSize: 17px;
    textAlign: center;
}

.what-we-do {
    marginBottom: 84px;
}
.what-we-do h1 {
    
    color: #184A88;
    fontSize: 44px;
    fontWeight: bold;
    textAlign: center;
}
.frame {
    padding: 35px 27px;
    border: 1px solid rgba(44,64,88,0.1);
    backgroundColor: #FFFFFF;
}
.group {
    marginBottom: 41px;
    textAlign: center;
}
.dyanamic-web-solutions h4 {
    fontWeight: 600;
    color: #4A4A4A;
    fontSize: 17px;
    textAlign: center;
}
.at-spineor-we-explore-learn-an {
    marginTop: 23px;
    minHeight: 85px;
  
}
.at-spineor-we-explore-learn-an p {
    lineHeight: 2;
    color: #4A4A4A;
    fontSize: 14px;
    fontWeight: 300;
    textAlign: center;
}
.view-more{
    marginTop: 24px;
    textAlign: center;
}
.view-more a {
    fontWeight: 600;
    fontSize: 14px;
    textDecoration: none;
    display: inline-block;
    border: 1px solid #184a88;
    paddingTop: 14px;
    paddingBottom: 14px;
    paddingLeft: 20px;
    paddingRight: 20px;
    width: 54%;
    color: #184a88;
}

.what-we-do h1:before{
    content:"";
    display: block;
    height: 1px;
    width: 100px;
    backgroundColor: #184A88;
    margin:auto;
    marginBottom: 30px;
}
.frame:hover{
    cursor: pointer;
boxShadow: 0 -1px 35px 3px rgba(7,7,7,0.07);
}
.frame:hover .view-more a {
    cursor: pointer;
    color: white;
    backgroundColor: #184A88;
}


      
      `}</style>
            </section>


        </div>
    )
}
export default Common
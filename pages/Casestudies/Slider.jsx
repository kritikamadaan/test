import Head from 'next/head'

export const config = { amp: true };

export default function Slider(props) {
    return (<div>
        <React.Fragment>

            <Head>
                <script
                    async
                    custom-element="amp-carousel"
                    src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
                <script async src="https://cdn.ampproject.org/v0.js"></script>

                <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
                <link rel="canonical" href="https://amp.dev/documentation/examples/components/amp-carousel/index.html" />
                {/*<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1" />*/}
            </Head>
            <body>


                <amp-carousel width="400"
                    height="300"
                    layout="responsive"
                    type="slides">
                    <amp-img src="/images/banner42.jpg"
                        width="400"
                        height="300"
                        layout="responsive"
                        alt="a sample image"></amp-img>
                    <amp-img src="/images/banner1_2.jpg"
                        width="400"
                        height="300"
                        layout="responsive"
                        alt="another sample image"></amp-img>
                    <amp-img src="/images/banner42.jpg"
                        width="400"
                        height="300"
                        layout="responsive"
                        alt="and another sample image"></amp-img>
                </amp-carousel>


            </body>
        </React.Fragment>




    </div>);






}
import React, { Component } from 'react';
import Head from "next/head";

class Accordion extends Component {
    render() {
        return (
            <div>
                <React.Fragment>
                    <Head>
                        <script async src="https://cdn.ampproject.org/v0.js"></script>
                        <script custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js" async></script>
                        <link rel="canonical" href="https://preview.amp.dev/documentation/components/amp-accordion.example.1.html" />
                        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1" />

                    </Head>
                    <body>
                        <amp-accordion id="my-accordion" disable-session-states>
                            <section>
                                <h2>Contact</h2>
                                <p>My contact form.</p>
                            </section>
                            <section>
                                <h2>about us</h2>
                                <div>My about us form.</div>
                            </section>
                            <section expanded>
                                <h2>blog Page</h2>
                                <amp-img src="/images/blg1.png"
                                    width="320"
                                    height="256"></amp-img>


                            </section>
                            <section expanded>
                                <h2>blog Page</h2>
                                <amp-img src="/images/blg2.png"
                                    width="320"
                                    height="256"></amp-img>


                            </section>
                        </amp-accordion>

                    </body>

                </React.Fragment>


            </div>
        );
    }
}

export default Accordion;

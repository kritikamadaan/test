import React, { Component } from 'react';
import Link from "next/link";


export default class Footer extends React.Component {
    render() {
        return (
            <div>

                <div className="container-fluid foo-image">

                    <div className="container">

                        <div className="row">

                            <div className="col-sm-3">

                                <div className="product-detail">

                                    <h4>PRODUCTS</h4>

                                    <div className="product-list">

                                        <ul>
                                            <li><Link href="/"><a>HOME</a></Link></li>
                                            <li><Link href="/about"><a>ABOUT US </a></Link></li>
                                            <li><Link href="/our-process"><a>OUR PROCESS</a></Link></li>
                                            <li><Link href="/career"><a>Careers</a></Link></li>
                                            <li><Link href="/case-studies"><a>CASE STUDIES</a></Link></li>
                                            <li><Link href="/Contact"><a>contact</a></Link></li>
                                            <li><Link href="/Blog"><a>BLOG</a></Link></li>

                                        </ul>

                                    </div>

                                </div>

                            </div>

                            <div className="col-sm-6">

                                <div className="product-detail">

                                    <h4>SERVICES</h4>

                                    <div className="product-list">

                                        <ul className="list-1">

                                            <li><Link href="/open-source-Implemetation"><a>OPEN SOURCE IMPLEMEMTATION</a></Link></li>
                                            <li><Link href="/java-scala-development"><a>SCALA PROGRAMMING</a></Link></li>
                                            <li><Link href="/restful-apis"><a>RESTFUL API</a></Link></li>
                                            <li><Link href="/automated-devops-development"><a>AUTOMATED DEVEOPS DEPLOYMENT</a></Link></li>
                                            <li><Link href="/big-data-analytics"><a>BIG DATA ANALYTIC</a></Link></li>
                                        </ul>

                                        <ul className="list-2">

                                            <li><Link href="/native-apps-building"><a>NATIVE APPS BUILDING</a></Link></li>
                                            <li><Link href="/full-mean-stack"><a>FULL/MEAN STACK DEVELOPMENT</a></Link></li>
                                            <li><Link href="/third-party-integration"><a>THIRD PARTY INTEGRATION </a></Link></li>
                                            <li><Link href="/Machine-learning"><a>MACHINE LEARNING</a></Link></li>
                                            <li><Link href="/java-scala-development"><a>JAVA PROGRAMMING</a></Link></li>
                                        </ul>

                                    </div>

                                </div>



                            </div>

                            <div className="col-sm-3 ">

                                <div className="product-detail right-align">

                                    <h4>CONTACT</h4>

                                    <div className="product-list">

                                        <ul>

                                            <li>+91(991)502-1146</li>

                                            <li>hr@spineor.com</li>


                                            <li><Link href="https://www.facebook.com/Spineor-Webservices-Pvt-Ltd-392137931232127/?modal=admin_todo_tour"><a><i className="fa fa-facebook-official"></i></a></Link>

                                                <Link href="https://www.linkedin.com/in/spineor/"><a><i className="fa fa-linkedin-square"></i></a></Link>

                                                <Link href="https://www.youtube.com/watch?v=eRldLjI9Mlo"><a><i className="fa fa-youtube-play"></i></a></Link>
                                            </li>

                                        </ul>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div className="foot-info">

                            <hr />

                            <p id="cpy">Copyright<span><i className="fa fa-copyright"></i></span>2018-SPINEOR</p>

                        </div>

                    </div>

                    <div className="upper-icon">

                        <a href="#top"><img src="images/arrow-bttn.png" /></a>

                    </div>

                </div>

                <style jsx>{`

                .foo-image{
    position: relative;
    background-image: url(../images/footer2.jpg);
    height: auto;
    background-size: cover;
    background-repeat: no-repeat;
}
.upper-icon {
    bottom: 60px;
    right: 126px;
    position: absolute;
}
.foo-image .product-detail {
    color: white;
}
.foo-image .product-list a {
    font-weight: 100;
   font-size: 14px;
   text-decoration: none;
   color: white;
   opacity: 0.8;
}

.list-1{
   display: inline-block;
}
.right-align{
   float: right;
}
.list-2{
   float: right;
   display: inline-block;
}
.product-detail .product-list ul {
    padding: 0;
    list-style-type: none;
}
.foo-image .product-detail h4{
    line-height: 4;
    font-size: 15px;
    color: rgb(255,255,255);
    font-weight: bold;

}
.product-detail .product-list li{
    line-height: 2.3;
    font-size: 14px;
    color: rgb(255, 255, 255);
 
}
.product-detail .product-list i{
    padding: 3px;
    font-size: 18px;
    opacity: 0.6;
}
.foo-image .product-list a:hover, .foo-image .product-list a:hover i {
   opacity: 1;
}
.foo-image .foot-info{
    position: relative;
    margin-top: 34px;
    color: white;
}
.foo-image .foot-info p#cpy{
    text-align: center;
    font-size: 10px;
}
.foo-image .foot-info span{

    padding: 3px;
}
.foo-image .foot-info p#cpy1{
    text-align: center;
}
footer {
    margin-top: 40px;
}
.mt0 {
    margin-top: 0 !important; 
}



      `}</style>
            </div>
        );
    }
}



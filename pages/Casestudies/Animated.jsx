import Head from 'next/head'

export const config = { amp: true };

export default function Slider(props) {
    return (<div>
        <React.Fragment>

            <Head>
                <script async src="https://cdn.ampproject.org/v0.js"></script>
                <script custom-element="amp-anim" src="https://cdn.ampproject.org/v0/amp-anim-0.1.js" async></script>
                <link rel="canonical" href="https://preview.amp.dev/documentation/guides-and-tutorials/develop/media_iframes_3p/index.example.4.html" />
                {/* <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1" />*/}

            </Head>
            <body>

                <amp-anim width="400"
                    height="300"
                    src="/static/inline-examples/images/wavepool.gif">
                    <amp-img placeholder
                        width="400"
                        height="300"
                        src="/static/inline-examples/images/wavepool.png">
                    </amp-img>
                </amp-anim>



            </body>
        </React.Fragment>




    </div>);






}
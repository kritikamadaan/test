import React, { Fragment } from "react";
import Head from "next/head";
//import img from '/static/assests/template/spineor/images/angular.png';


export const config = { amp: true }

function Services(props) {
    return (
        <Fragment>

            <amp-3d-gltf layout="fixed"
                width="320"
                height="240"
                alpha="true"
                antialiasing="true"
                autorotate="true"
                enablezoom="false"
                src="/images/_tec.jpg"></amp-3d-gltf>
            <div>
                <amp-carousel height="300" layout="fixed-height" type="carousel">
                    <amp-img src="/images/banner.jpg" width="400" height="300" alt="a sample image"></amp-img>
                    <amp-img src="/images/banner.jpg" width="400" height="300" alt="another sample image"></amp-img>
                    <amp-img src="/images/banner.jpg" width="400" height="300" alt="and another sample image"></amp-img>
                </amp-carousel>


            </div>
            <amp-carousel layout="fixed-height" height="250" type="carousel" >
                <amp-img src="/images/banner.jpg" width="300" height="250"></amp-img>

                <amp-ad width="300" height="250"
                    type="doubleclick"
                    data-slot="/35096353/amptesting/image/static">
                    <div placeholder>This ad is still loading.</div>
                </amp-ad>

                <amp-fit-text width="300" height="250" layout="fixed">
                    Big, bold article quote goes here.
    </amp-fit-text>
            </amp-carousel>



            <amp-carousel height="300" layout="fixed-height" type="slides">
                <div>
                    <div class="blue-box">
                        This is a blue box.
      </div>
                </div>
                <div>
                    <div class="red-box">
                        This is a red box.
      </div>
                </div>
                <div>
                    <div class="green-box">
                        This is a green box.
      </div>
                </div>
            </amp-carousel>


            <amp-carousel layout="fixed-height" height="168" type="carousel" >
                <amp-img src="/images/banner.jpg" width="300" height="168"></amp-img>
                <amp-img src="/images/banner_Automated.jpg" width="300" height="168"></amp-img>
                <amp-img src="/images/banne_r3.jpg" width="300" height="168"></amp-img>
            </amp-carousel>




            <amp-sidebar id="sidebar1" layout="nodisplay" side="left">
                <div role="button" aria-label="close sidebar" on="tap:sidebar1.toggle" tabindex="0" class="close-sidebar">✕</div>
                <ul class="sidebar">
                    <li><a href="/">Example 1</a></li>
                    <li><a href="/">Example 2</a></li>
                    <li><a href="#">Example 3</a></li>
                </ul>
            </amp-sidebar>
            <header class="headerbar">
                <a href="homepage.html">
                    <amp-img class="home-button" src="images/About_top_img.jpg" width="100" height="100"></amp-img>
                </a>
                <div class="site-name">News Site</div>
            </header>

            <amp-facebook width="552" height="303"
                layout="responsive"
                data-href="https://www.facebook.com/zuck/posts/10102593740125791">
            </amp-facebook>

            <amp-anim width="245"
                height="300"
                src="/images/_tec.png"
                alt="an animation"
                attribution="The Go gopher was designed by Reneee French and is licensed under CC 3.0 attributions.">
            </amp-anim>

            <amp-ad data-slot="/30497360/a4a/a4a_native"
                height="250"
                type="doubleclick"
                width="300"></amp-ad>
            <button amp-access="NOT loggedIn"
                amp-access-hide
                on="tap:amp-access.login-sign-in">Login to comment</button>

            <amp-list
                width="auto"
                height="100"
                layout="fixed-height"
                src="/static/inline-examples/data/amp-list-urls.json"
            />

            <section poool-access-preview amp-access="NOT access">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
                    ullamcorper turpis vel commodo scelerisque.
  </p>
            </section>

            <section poool-access-content amp-access="access" amp-access-hide>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
                    ullamcorper turpis vel commodo scelerisque.
  </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
                    ullamcorper turpis vel commodo scelerisque.
  </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
                    ullamcorper turpis vel commodo scelerisque.
  </p>
            </section>

            <section amp-access="NOT error AND NOT access" id="poool"></section>

            <button on="tap:carousel-macro.execute(foo=1, bar=2)">
                Go to slide 1 then 2
                </button>



            <div>
                <header className="headerbar">
                    <a href="homepage.html">
                        <amp-img height="10 px" width="5 px" className="home-button" src="icons/home.png" width="36" height="36"></amp-img>
                    </a>
                    <div className="site-name">News Site</div>
                </header>
            </div>
            <amp-audio
                width="400"
                height="300"
                src="https://yourhost.com/audios/myaudio.mp3"
            >
                <div fallback>
                    <p>Your browser doesn’t support HTML5 audio</p>
                </div>
                <source type="audio/mpeg" src="foo.mp3" />
                <source type="audio/ogg" src="foo.ogg" />
            </amp-audio>


            <amp-anim width="400" height="300" src="my-gif.gif">
                <amp-img placeholder width="400" height="300" src="my-gif-screencap.jpg">
                </amp-img>
            </amp-anim>


            <amp-ad
                type="a9"
                data-amzn_assoc_ad_mode="auto"
                data-divid="amzn-assoc-ad-fe746097-f142-4f8d-8dfb-45ec747632e5"
                data-recomtype="async"
                data-adinstanceid="fe746097-f142-4f8d-8dfb-45ec747632e5"
                width="300"
                height="250"
                data-aax_size="300x250"
                data-aax_pubname="test123"
                data-aax_src="302"
            >
            </amp-ad>
            <amp-ad
                width="300"
                height="250"
                type="industrybrains"
                data-width="300"
                data-height="250"
                data-cid="19626-3798936394"
            >
            </amp-ad>
            <amp-embed
                type="taboola"
                width="400"
                height="300"
                layout="responsive"
                data-publisher="amp-demo"
                data-mode="thumbnails-a"
                data-placement="Ads Example"
                data-article="auto"
            >
            </amp-embed>
        </Fragment >
    );
}

export default Services
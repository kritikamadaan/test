import fetch from 'isomorphic-unfetch';
import Layout from '../components/layout';
import Link from 'next/link'

export default function Sitemaps(props){
    return (
        <Layout>
            {
                props.shows.map((show, i) => (
                    <div key={i}>
                        <Link href="/p/[id]" as={`/p/${show.id}`}>
                            <a>{show.name}</a>
                        </Link>
                    </div>
                ))
            }
        </Layout>
    )
}
Sitemaps.getInitialProps = async function () {
    const res = await fetch('https://api.tvmaze.com/search/shows?q=batman');
    const data = await res.json();

    console.log(`Show data fetched. Count: ${data.length}`, data.map(entry => entry.show));

    return {
        shows: data.map(entry => entry.show)
    };
};
import Link from 'next/link';

const linkStyle = {
    marginRight: 15
};
const Header = () => (
    <div>
        <Link href="/">
            <a style={linkStyle}>Home</a>
        </Link>
        <Link href="/Casestudies">
            <a style={linkStyle}>Casestudies</a>
        </Link>
        <Link href="/Services">
            <a style={linkStyle}>Services</a>
        </Link>

        <Link href="/property">
            <a style={linkStyle}>Property</a>
        </Link>
        <Link href="/sitemaps">
            <a style={linkStyle}>Sitemap</a>
        </Link>
        <Link href="/propertyDetail?author=rauch">
            <a style={linkStyle}>Property Detail</a>
        </Link>


    </div>
)

export default Header;